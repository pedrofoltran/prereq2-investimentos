package com.br.investimentos.controllers;

import com.br.investimentos.models.Cliente;
import com.br.investimentos.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
    @Autowired
    ClienteService clienteService;

    @PostMapping
    public Cliente adicionarCliente(@RequestBody @Valid Cliente cliente){
        return clienteService.adicionarCliente(cliente);
    }

}
