package com.br.investimentos.controllers;

import com.br.investimentos.models.DTOs.RespostaSimulacaoDTO;
import com.br.investimentos.models.Simulacao;
import com.br.investimentos.repositories.SimulacaoRepository;
import com.br.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    public Iterable<Simulacao> buscarTodasSimulacoes(){
        return simulacaoService.buscarTodas();

    }


}
