package com.br.investimentos.controllers;

import com.br.investimentos.models.DTOs.EntradaSimulacaoDTO;
import com.br.investimentos.models.DTOs.RespostaSimulacaoDTO;
import com.br.investimentos.models.Investimento;
import com.br.investimentos.models.Simulacao;
import com.br.investimentos.services.InvestimentoService;
import com.br.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.criteria.CriteriaBuilder;
import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento adicionarInvestimento(@RequestBody @Valid Investimento investimento){
        investimentoService.salvarNovoInvestimento(investimento);
        return investimento;
    }

    @GetMapping()
    public Iterable<Investimento> buscarTodosInvestimentos(){
        return investimentoService.buscarTodos();
    }

    @PostMapping("/{id}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaSimulacaoDTO geraSimulacao(@PathVariable int id,
                                              @RequestBody @Valid EntradaSimulacaoDTO entradaSimulacaoDTO){
       try {
           return simulacaoService.gerarNovaSimulacao(id, entradaSimulacaoDTO);
       } catch  (RuntimeException runtimeException){
           throw new ResponseStatusException(HttpStatus.BAD_REQUEST, runtimeException.getMessage());
       }
    }


}
