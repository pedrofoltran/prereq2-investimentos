package com.br.investimentos.repositories;

import com.br.investimentos.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer> {
}
