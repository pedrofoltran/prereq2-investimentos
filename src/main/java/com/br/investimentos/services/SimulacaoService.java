package com.br.investimentos.services;

import com.br.investimentos.models.Cliente;
import com.br.investimentos.models.DTOs.EntradaSimulacaoDTO;
import com.br.investimentos.models.DTOs.RespostaSimulacaoDTO;
import com.br.investimentos.models.Investimento;
import com.br.investimentos.models.Simulacao;
import com.br.investimentos.repositories.ClienteRepository;
import com.br.investimentos.repositories.SimulacaoRepository;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class SimulacaoService {
    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private ClienteService clienteService;

    public RespostaSimulacaoDTO gerarNovaSimulacao(int id, EntradaSimulacaoDTO entradaSimulacaoDTO) {
        Investimento investimento;
        Cliente cliente;
        RespostaSimulacaoDTO respostaSimulacaoDTO;
        Simulacao simulacao = new Simulacao();

        investimento = investimentoService.buscarPorID(id);
        simulacao.setInvestimento(investimento);

        cliente =  clienteService.buscarPorID(entradaSimulacaoDTO.getIdCliente());
        simulacao.setCliente(cliente);

        simulacao.setMesesSimulado(entradaSimulacaoDTO.getMesesSimulado());
        simulacao.setMontanteSimulado(entradaSimulacaoDTO.getMontanteSimulado());

        simulacaoRepository.save(simulacao);

        respostaSimulacaoDTO = new RespostaSimulacaoDTO(investimento.getRendimentoInvestimento(),
                calcularSimulacao(investimento, simulacao));

        return respostaSimulacaoDTO;
    }

    public BigDecimal calcularSimulacao(Investimento investimento, Simulacao simulacao) {
        double montante;
        double rendimento;
        int meses;
        BigDecimal montanteConvertido;

        montante = simulacao.getMontanteSimulado();
        rendimento = investimento.getRendimentoInvestimento();
        meses = simulacao.getMesesSimulado();

        montante += (rendimento / 100) * montante * meses;
        montanteConvertido = new BigDecimal(montante);

        return montanteConvertido.setScale(2, BigDecimal.ROUND_HALF_EVEN);

    }

    public Iterable<Simulacao> buscarTodas() {
        return simulacaoRepository.findAll();
    }
}
