package com.br.investimentos.services;


import com.br.investimentos.models.Investimento;
import com.br.investimentos.repositories.InvestimentoRepository;
import jdk.nashorn.internal.runtime.regexp.joni.constants.OPCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Investimento salvarNovoInvestimento(Investimento investimento){
        return investimentoRepository.save(investimento);
    }

    public Iterable<Investimento> buscarTodos() {
        return investimentoRepository.findAll();
    }

    public Investimento buscarPorID(int id){
        Optional<Investimento> optionalInvestimento = investimentoRepository.findById(id);
        if(optionalInvestimento.isPresent())
            return optionalInvestimento.get();
        else
            throw new RuntimeException("Investimento não encontrado");
    }
}
