package com.br.investimentos.services;

import com.br.investimentos.models.Cliente;
import com.br.investimentos.models.Investimento;
import com.br.investimentos.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    ClienteRepository clienteRepository;

    public Cliente adicionarCliente(Cliente cliente) {
        return clienteRepository.save(cliente);

    }

    public Cliente buscarPorID(int idCliente) {
        Optional<Cliente> optionalCliente = clienteRepository.findById(idCliente);
        if (optionalCliente.isPresent())
            return optionalCliente.get();
        else
            throw new RuntimeException("Cliente não encontrado");
    }
}
