package com.br.investimentos.models.DTOs;

import java.math.BigDecimal;

public class RespostaSimulacaoDTO {
    private double rendimentoPorMes;
    private BigDecimal Montante;

    public RespostaSimulacaoDTO() {
    }

    public RespostaSimulacaoDTO(double rendimentoPorMes, BigDecimal montante) {
        this.rendimentoPorMes = rendimentoPorMes;
        Montante = montante;
    }

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public BigDecimal getMontante() {
        return Montante;
    }

    public void setMontante(BigDecimal montante) {
        Montante = montante;
    }
}
