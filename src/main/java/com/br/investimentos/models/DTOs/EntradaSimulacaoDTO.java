package com.br.investimentos.models.DTOs;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;

public class EntradaSimulacaoDTO {
    private int idCliente;
    @Digits(integer = 1000000, fraction = 2, message = "Quantidade de digitos do rendimento é invalido")
    private Double montanteSimulado;
    @Min(value = 0, message = "Numero de meses maior que 0")
    private int mesesSimulado;

    public EntradaSimulacaoDTO(int idCliente, Double montanteSimulado, int mesesSimulado) {
        this.idCliente = idCliente;
        this.montanteSimulado = montanteSimulado;
        this.mesesSimulado = mesesSimulado;
    }

    public EntradaSimulacaoDTO() {
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public Double getMontanteSimulado() {
        return montanteSimulado;
    }

    public void setMontanteSimulado(Double montanteSimulado) {
        this.montanteSimulado = montanteSimulado;
    }

    public int getMesesSimulado() {
        return mesesSimulado;
    }

    public void setMesesSimulado(int mesesSimulado) {
        this.mesesSimulado = mesesSimulado;
    }
}
