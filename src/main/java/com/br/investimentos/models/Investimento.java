package com.br.investimentos.models;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idInvestimento;

    @NotBlank(message = "Nome do investimento não pode ser vazio")
    @NotNull(message = "Nome do investimento não pode ser nulo")
    @Column(unique = true)
    private String nomeInvestimento;
    @Digits(integer = 1000000, fraction = 2, message = "Quantidade de digitos do rendimento é invalido")
    private Double rendimentoInvestimento;

    public Investimento() {
    }

    public int getIdInvestimento() {
        return idInvestimento;
    }

    public void setIdInvestimento(int idInvestimento) {
        this.idInvestimento = idInvestimento;
    }

    public String getNomeInvestimento() {
        return nomeInvestimento;
    }

    public void setNomeInvestimento(String nomeInvestimento) {
        this.nomeInvestimento = nomeInvestimento;
    }

    public Double getRendimentoInvestimento() {
        return rendimentoInvestimento;
    }

    public void setRendimentoInvestimento(Double rendimentoInvestimento) {
        this.rendimentoInvestimento = rendimentoInvestimento;
    }
}
