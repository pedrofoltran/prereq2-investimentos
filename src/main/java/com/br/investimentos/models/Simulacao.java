package com.br.investimentos.models;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;

@Entity
public class Simulacao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idSimulacao;

    @Digits(integer = 1000000, fraction = 2, message = "Quantidade de digitos do rendimento é invalido")
    private Double montanteSimulado;
    @Min(value = 0, message = "Numero de meses maior que 0")
    private int mesesSimulado;
    @ManyToOne(cascade = CascadeType.ALL)
    private Investimento investimento;
    @ManyToOne(cascade = CascadeType.ALL)
    private Cliente cliente;

    public Simulacao() {
    }

    public int getIdSimulacao() {
        return idSimulacao;
    }

    public void setIdSimulacao(int idSimulacao) {
        this.idSimulacao = idSimulacao;
    }


    public Double getMontanteSimulado() {
        return montanteSimulado;
    }

    public void setMontanteSimulado(Double montanteSimulado) {
        this.montanteSimulado = montanteSimulado;
    }

    public int getMesesSimulado() {
        return mesesSimulado;
    }

    public void setMesesSimulado(int mesesSimulado) {
        this.mesesSimulado = mesesSimulado;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
