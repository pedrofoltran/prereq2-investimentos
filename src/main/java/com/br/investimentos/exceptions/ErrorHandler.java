package com.br.investimentos.exceptions;

import com.br.investimentos.exceptions.errors.MensagemErro;
import com.br.investimentos.exceptions.errors.ObjetoErro;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public MensagemErro manipulacaoErroValidacao(MethodArgumentNotValidException exception) {
        HashMap<String, ObjetoErro> erros = new HashMap<>();
        BindingResult resultado = exception.getBindingResult();

        for (FieldError erro : resultado.getFieldErrors()) {
            erros.put(erro.getField(), new ObjetoErro(erro.getDefaultMessage()));
        }

        MensagemErro mensagemDeErro = new MensagemErro(HttpStatus.UNPROCESSABLE_ENTITY.toString(),
                "Erro de validação dos campos enviados", erros);
        return mensagemDeErro;
    }
}

