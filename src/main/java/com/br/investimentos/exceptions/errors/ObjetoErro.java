package com.br.investimentos.exceptions.errors;

public class ObjetoErro {
    private String mensagemErro;

    public ObjetoErro(String mensagemErro) {
        this.mensagemErro = mensagemErro;
    }

    public ObjetoErro() {
    }

    public String getMensagemErro() {
        return mensagemErro;
    }

    public void setMensagemErro(String mensagemErro) {
        this.mensagemErro = mensagemErro;
    }
}
