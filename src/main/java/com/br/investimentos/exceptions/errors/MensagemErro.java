package com.br.investimentos.exceptions.errors;

import java.util.HashMap;

public class MensagemErro {
    private String erro;
    private String mensagemErro;
    private HashMap<String, ObjetoErro> camposErro;

    public MensagemErro(String erro) {
        this.erro = erro;
    }

    public MensagemErro(String erro, String mensagemErro, HashMap<String, ObjetoErro> camposErro) {
        this.erro = erro;
        this.mensagemErro = mensagemErro;
        this.camposErro = camposErro;
    }

    public String getErro() {
        return erro;
    }

    public void setErro(String erro) {
        this.erro = erro;
    }

    public String getMensagemErro() {
        return mensagemErro;
    }

    public void setMensagemErro(String mensagemErro) {
        this.mensagemErro = mensagemErro;
    }

    public HashMap<String, ObjetoErro> getCamposErro() {
        return camposErro;
    }

    public void setCamposErro(HashMap<String, ObjetoErro> camposErro) {
        this.camposErro = camposErro;
    }

}
